<?php include_once 'header.php'; ?>

<?php
include_once 'secureLogin/includes/register.inc.php';
include_once 'secureLogin/includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="secureLogin/js/sha512.js"></script> 
        <script type="text/JavaScript" src="secureLogin/js/forms.js"></script>
        <link rel="stylesheet" href="styles/main.css" />
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    </head>
    <body>
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <div class="container">
        	<div id="register" class="row" style=" margin-top: 40px;">
            	<div class="col-md-6">
                	<form class="form-group" action="secureLogin/includes/register.inc.php" method="post" name="registration_form">
                        <label>Username</label> <input class="form-control" type='text' name='username' id='username' />
                        <label>Email</label> <input class="form-control" type="text" name="email" id="email" /><br>
                        <label>Password</label> <input class="form-control" type="password" name="password" id="password"/><br>
                        <label>Confirm password</label> <input class="form-control" type="password" name="confirmpwd" id="confirmpwd" /><br>
                        <input type="submit" class="btn btn-lg btn-success" value="Register" onClick="return regformhash(this.form, this.form.username, this.form.email, this.form.password,
                         this.form.confirmpwd);" /> 
        			</form>
        <p class="h4">Return to the <a href="index.php">login page</a>.</p>
        
                </div>
                <div class="col-md-6">
            		<div>
                    	<img class="img-circle img-responsive" src="airport.jpg">
                    </div>
                </div>
            </div>
        </div>
        <h1>Register with us</h1>
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        
        
        
        
        
        
    </body>
    <?php include_once 'footer.php'; ?>

</html>