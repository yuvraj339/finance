<?php  	include_once 'header.php'; ?>
 
<?php
 
include_once 'secureLogin/includes/db_connect.php';
include_once 'secureLogin/includes/functions.php';
 
/*sec_session_start();*/
 
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" href="styles/main.css" />
        <script type="text/JavaScript" src="secureLogin/js/sha512.js"></script> 
        <script type="text/JavaScript" src="secureLogin/js/forms.js"></script> 
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
 
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?>
        <div class="container">
        	<div class="row" style="margin-bottom:80px; margin-top:35px;">
            	<div class="col-md-6">
                	<div class="form-group">
                    	<form action="secureLogin/includes/process_login.php" method="post" name="login_form">                      
            				<label>Email</label> 
                            <input type="text" class="form-control" name="email" /><!--Wasim12-->
                            <label>Password</label>
            				<input type="password" class="form-control" name="password" id="password" /><br>
            				<input type="button" value="Login" class="btn btn-info form-control" onClick="formhash(this.form, this.form.password);" /> 
        				</form>
                    </div>
                    <p class="h4">If you don't have a login, please <a href="register.php">register</a></p>
                    <p class="h4">If you are done, please <a href="logout.php">log out</a>.</p>
                    <p class="h4">You are currently logged <?php echo $logged ?>.</p>
                    
                </div>
                <div class="col-md-6">
                	<div>
                    	<img class="img-responsive" src="upload/5.jpg">
                    </div>
                </div>
            </div>
        </div> 
        
          
    </body>
</html>

  <?php include_once 'footer.php'; ?>
