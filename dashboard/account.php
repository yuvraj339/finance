<?php include_once './dash_header.php';
 include_once './left_sidebar.php'; ?>
     
     <div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Dashboard : <small>Manage Account Section </small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Transaction Information 
                       </div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Total Balance</a>
                                </li>
                                <li class=""><a href="#profile" data-toggle="tab">Today Transaction</a>
                                </li>
                                <li class=""><a href="#messages" data-toggle="tab">History</a>
                                </li>
                                <li class=""><a href="#settings" data-toggle="tab">Gifts</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="home">
                                    <h4>Total Balance</h4>
                                    <p>Pocket : $10000</p>
                                </div>
                                <div class="tab-pane fade" id="profile">
                                    <h4>Today Transaction</h4>
                                    <p>xyz</p>
                                </div>
                                <div class="tab-pane fade" id="messages">
                                    <h4>History</h4>
                                    <p>xyz</p>
                                </div>
                                <div class="tab-pane fade" id="settings">
                                    <h4>Gift From Us</h4>
                                    <p>You Got : <strong> $10000 </strong></p>
                                    <p>Date : <strong> <?php  echo date("l jS \of F Y h:i:s A") ;?> </strong></p>
                                    <p>Valid Till : <strong> 12/10/16 </strong></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            </div>
        </div>
    </div>
</div>
     
     <?php
 include_once './dash_footer.php';
