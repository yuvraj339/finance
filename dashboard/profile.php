<?php
include_once './dash_header.php';
include_once './left_sidebar.php';
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Dashboard : <small>User profile</small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <form method="post" action="#">
                    <div class="form-group"><input class="form-control" placeholder="User name" name="uname" ></div>
                    <div class="form-group"><input class="form-control" placeholder="Email" name="email" ></div>
                    <div class="form-group"><input class="form-control" placeholder="Contact no" name="contact" ></div>
                    <button type="submit" class="btn btn-default">Submit Button</button>
                    <button type="reset" class="btn btn-default">Reset Button</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once './dash_footer.php'; ?>